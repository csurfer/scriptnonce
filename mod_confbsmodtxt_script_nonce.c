#include "httpd.h"
#include "http_config.h"
#include "http_core.h"

#include "httpd.h"
#include "http_config.h"
#include "apr_buckets.h"
#include "apr_general.h"
#include "apr_lib.h"
#include "util_filter.h"
#include "http_request.h"
#include "apr_tables.h"

#include <ctype.h>
#include <stdio.h>
#include <rand.h>
#include <apr_strmatch.h>

#define DEFAULT_NONCE_LEN 20

/* config stuff */
typedef struct {
    apr_array_header_t *magic;
} script_nonce_dir_conf;

typedef struct magic_pattern_t {
    const apr_strmatch_pattern *pattern;
    const char *replacement;
    apr_size_t noncelen;
    apr_size_t patlen;
} magic_pattern_t;
/* config stuff */

module AP_MODULE_DECLARE_DATA script_nonce_module;

static const char s_szscript_nonce_name[]="script_nonce_filter";
static const char trial_nonce[] = "THISISNONCE-NSE";

/* config stuff */

const char* script_nonce_cmd_func(cmd_parms* cmd, void* cfg, const char* pattern, const char* lenstr){
  magic_pattern_t* magic_pattern;
  
  magic_pattern = apr_array_push(((script_nonce_dir_conf *) cfg)->magic);

  /* initializing the structure */
  magic_pattern->patlen = strlen(pattern);
  magic_pattern->pattern = apr_strmatch_precompile(cmd->pool, pattern, 
                                                   TRUE);
  magic_pattern->noncelen = atoi(lenstr);

  return NULL;
}


static const command_rec script_nonce_cmds[] = {
  AP_INIT_TAKE2("MAGICSTRING", script_nonce_cmd_func, NULL, OR_ALL,
  "Magic string that is replaced with nonce and length"),
  { NULL }
};

static void* scrip_nonce_create_dir_conf(apr_pool_t* pool, char* x) {
  script_nonce_dir_conf* dir = apr_pcalloc(pool, sizeof(script_nonce_dir_conf));
  /* Set up the default values for fields of dir */
  dir->magic = apr_array_make(pool, 10, sizeof(magic_pattern_t));
  return dir ;
}


/* config stuff */


static void script_nonce_insert_filter(request_rec *r)
{
      ap_add_output_filter(s_szscript_nonce_name,NULL,r,r->connection);
}

char* sc_add_header(request_rec *r, int noncelen) {
    char value[200];
    char strNonce[100];

    int byteslen;

    byteslen = (noncelen*6/8 + 1);  

    apr_generate_random_bytes(strNonce, byteslen);
    char encodedNonce[51];
    apr_base64_encode(encodedNonce, (const char *)strNonce, strlen(strNonce));

    char actualNonce[51];
    strncpy(actualNonce, encodedNonce, noncelen);
    actualNonce[noncelen] = '\0';

    sprintf(value, "default-src 'self'; script-src 'self' 'unsafe-inline'; reflected-xss allow; script-nonce %s", actualNonce);

    if(r){
        apr_table_set(r->headers_out, "Content-Security-Policy", value);
        return actualNonce;
    }
    return "";
}

/* modify this function to insert the nonce using this http://www.apachetutor.org/dev/brigades*/
static apr_status_t script_nonce_out_filter(ap_filter_t *f,
                                        apr_bucket_brigade *bb)
{
  //char *pat_text = "$@$";
  char replacement[51];  

  /* config stuff */
  script_nonce_dir_conf *cfg =
  (script_nonce_dir_conf *) ap_get_module_config(f->r->per_dir_config,
                                             &script_nonce_module);
  magic_pattern_t *magic_pattern;
    
  magic_pattern = (magic_pattern_t *) cfg->magic->elts;

  /* config stuff */
  int noncelen = magic_pattern->noncelen;
  
  if(noncelen <= 0 || noncelen > 50){
    noncelen = DEFAULT_NONCE_LEN;
  }
  
  strcpy(replacement, sc_add_header(f->r, noncelen));

  //const apr_strmatch_pattern* pattern =  apr_strmatch_precompile(f->r->pool, pat_text, 0);

  if (APR_BRIGADE_EMPTY(bb)) {
     return APR_SUCCESS;
  }
  
  //size_t patlen = strlen(pat_text);
  size_t patlen = magic_pattern->patlen;
  size_t replen = strlen(replacement);

  apr_bucket* b ;
  apr_bucket* tmp_b;

  for ( b = APR_BRIGADE_FIRST(bb);
  b != APR_BRIGADE_SENTINEL(bb);
  b = APR_BUCKET_NEXT(b) ) {

    const char* buf ;
    size_t bytes;
    size_t rem_len;

    if (APR_BUCKET_IS_EOS(b)) {
      continue;
    }
    
    if ( apr_bucket_read(b, &buf, &bytes, APR_BLOCK_READ)
            == APR_SUCCESS ) {
      
      /* We have a bucket full of text.  Just escape it where necessary */
      size_t count = 0 ;
      const char* p = buf;
      char *match = NULL;
      rem_len = bytes;
      
      while ( count < bytes ) {
      //while(1) {
        //size_t sz = strcspn(p, "<>&\"") ;
        match = apr_strmatch(magic_pattern->pattern, p, rem_len);
        //match = strstr(p, pat_text);

        if(match){
          size_t sz = (size_t)(match - p);

          count += sz;

          if ( count < bytes ) {
            apr_bucket_split(b, sz) ; //Split off before buffer
            b = APR_BUCKET_NEXT(b) ;  //Skip over before buffer
            tmp_b = apr_bucket_transient_create(replacement, replen, f->r->connection->bucket_alloc);
            APR_BUCKET_INSERT_BEFORE(b, tmp_b); //Insert the replacement
            apr_bucket_split(b, patlen);  //Split off the pattern to remove
            //APR_BUCKET_REMOVE(b);   //... and remove it
            apr_bucket_delete(b);
	    b = APR_BUCKET_NEXT(b);  //Move cursor on to what-remains
                  //so that it stays in sequence with
                  //our main loop
            //int inc = abs(replen - patlen);
            /*
            while(1){
              ;
            }
            */
            count += patlen; //inc;
            p += (sz + patlen); //inc;
            rem_len = (bytes - count);
            match = NULL;
          }
        }
        else{
          break;
        }
      }
    }
  }
  return ap_pass_brigade(f->next, bb);
}



static void script_nonce_register_hooks(apr_pool_t *p) {
    ap_hook_insert_filter(script_nonce_insert_filter,NULL,NULL,APR_HOOK_LAST);
    ap_register_output_filter(s_szscript_nonce_name,script_nonce_out_filter,NULL,
                              AP_FTYPE_RESOURCE);
}

module AP_MODULE_DECLARE_DATA script_nonce_module = {
    STANDARD20_MODULE_STUFF,
    scrip_nonce_create_dir_conf,/* per-directory config creator */
    NULL,                     /* dir config merger */
    NULL,                     /* server config creator */
    NULL,                     /* server config merger */
    script_nonce_cmds,        /* command table */
    script_nonce_register_hooks, /* set up other request processing hooks */
};
