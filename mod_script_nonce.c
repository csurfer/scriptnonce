#include "httpd.h"
#include "http_config.h"
#include "http_core.h"

#include "httpd.h"
#include "http_config.h"
#include "apr_buckets.h"
#include "apr_general.h"
#include "apr_lib.h"
#include "util_filter.h"
#include "http_request.h"

#include "apr_md5.h"

#include <ctype.h>
#include <stdio.h>
#include <rand.h>

module AP_MODULE_DECLARE_DATA script_nonce_module;

static const char s_szscript_nonce_name[]="script_nonce_filter";
static const char trial_nonce[] = "THISISNONCE-NSE";

static void script_nonce_insert_filter(request_rec *r)
{
    ap_add_output_filter(s_szscript_nonce_name,NULL,r,r->connection);
}

void sc_add_header(request_rec *r) {
    char value[200];
    char nonce[] = "01234567890123456789012345678901234";
    char repl_new[40], repl_nonce[40];
    sprintf(repl_nonce, "%s", nonce);
    strcpy(repl_new, repl_nonce);
    //apr_md5_encode(repl_nonce, repl_nonce, repl_new, strlen(repl_nonce));
    sprintf(value, "default-src 'self'; script-src 'self' 'unsafe-inline'; reflected-xss allow; script-nonce %s", repl_new);

    if(r){
        apr_table_set(r->headers_out, "Content-Security-Policy", value);
        //return nonce;
    }
    //return DECLINED;
}

/* modify this function to insert the nonce using this http://www.apachetutor.org/dev/brigades*/
static apr_status_t script_nonce_out_filter(ap_filter_t *f, apr_bucket_brigade *pbbIn)
{
    char repl[] = "a012345678901234567890123456789012a";
    char repl_new[40], repl_nonce[40];
     
    /*int nonce;
    if((nonce = sc_add_header(f->r)) == DECLINED)
    {
      ;
    }*/
    char nonce[] = "01234567890123456789012345678901234";
    sc_add_header(f->r);
    sprintf(repl_nonce, "%d", nonce);
    strcpy(repl_new, repl_nonce);
    //apr_md5_encode(repl_nonce, repl_nonce, repl_new, strlen(repl_nonce));

    request_rec *r = f->r;
    conn_rec *c = r->connection;
    apr_bucket *pbktIn;
    apr_bucket_brigade *pbbOut;

    pbbOut=apr_brigade_create(r->pool, c->bucket_alloc);
    for (pbktIn = APR_BRIGADE_FIRST(pbbIn);
         pbktIn != APR_BRIGADE_SENTINEL(pbbIn);
         pbktIn = APR_BUCKET_NEXT(pbktIn))
    {
        const char *data;
        apr_size_t len;
        char *buf;
        apr_size_t n;
        apr_bucket *pbktOut;

        if(APR_BUCKET_IS_EOS(pbktIn))
        {
            apr_bucket *pbktEOS=apr_bucket_eos_create(c->bucket_alloc);
            APR_BRIGADE_INSERT_TAIL(pbbOut,pbktEOS);
            continue;
        }

        /* read */
        apr_bucket_read(pbktIn,&data,&len,APR_BLOCK_READ);


        /* write */
        buf = apr_bucket_alloc(len, c->bucket_alloc);
        
        memcpy(buf, data, len);

        char *start = strstr((char *)buf, repl);
        while(start != NULL)
        {
          memcpy(start, repl_new, strlen(repl_new));
          start = strstr((char *)buf, repl);
        }

        pbktOut = apr_bucket_heap_create(buf, len, apr_bucket_free,
                                         c->bucket_alloc);
        APR_BRIGADE_INSERT_TAIL(pbbOut,pbktOut);
    }
    apr_brigade_cleanup(pbbIn);
    return ap_pass_brigade(f->next,pbbOut);
}



static void script_nonce_register_hooks(apr_pool_t *p) {
    ap_hook_insert_filter(script_nonce_insert_filter,NULL,NULL,APR_HOOK_LAST);
    ap_register_output_filter(s_szscript_nonce_name,script_nonce_out_filter,NULL,
                              AP_FTYPE_RESOURCE);
}

module AP_MODULE_DECLARE_DATA script_nonce_module = {
    STANDARD20_MODULE_STUFF,
    NULL,                     /* per-directory config creator */
    NULL,                     /* dir config merger */
    NULL,                     /* server config creator */
    NULL,                     /* server config merger */
    NULL,                     /* command table */
    script_nonce_register_hooks, /* set up other request processing hooks */
};
