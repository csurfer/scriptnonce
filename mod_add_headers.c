#include "httpd.h"
#include "http_config.h"
#include "http_core.h"

module AP_MODULE_DECLARE_DATA add_headers_module;

static int add_header(request_rec *r) {
    if(r){
        apr_table_set(r->headers_out, "MyKey", "Somevalue");
        return OK;
    }
    //return DECLINED;
}

static void add_headers_register_hooks(apr_pool_t *p) {
    //ap_hook_fixups(add_header, NULL, NULL, APR_HOOK_MIDDLE);
    ap_hook_fixups(add_header, NULL, NULL, APR_HOOK_LAST);
}

module AP_MODULE_DECLARE_DATA add_headers_module = {
    STANDARD20_MODULE_STUFF,
    NULL,                     /* per-directory config creator */
    NULL,                     /* dir config merger */
    NULL,                     /* server config creator */
    NULL,                     /* server config merger */
    NULL,                     /* command table */
    add_headers_register_hooks, /* set up other request processing hooks */
};
