#include "httpd.h"
#include "http_config.h"
#include "http_core.h"

#include "httpd.h"
#include "http_config.h"
#include "apr_buckets.h"
#include "apr_general.h"
#include "apr_lib.h"
#include "util_filter.h"
#include "http_request.h"

#include <ctype.h>
#include <stdio.h>
#include <rand.h>
#include <apr_strmatch.h>

module AP_MODULE_DECLARE_DATA script_nonce_module;

static const char s_szscript_nonce_name[]="script_nonce_filter";
static const char trial_nonce[] = "THISISNONCE-NSE";

static void script_nonce_insert_filter(request_rec *r)
{
    ap_add_output_filter(s_szscript_nonce_name,NULL,r,r->connection);
}

char* sc_add_header(request_rec *r) {
    char value[100];
    char strNonce[20];
    apr_generate_random_bytes(strNonce, 20);
    char encodedNonce[50];
    apr_base64_encode(encodedNonce, (const char *)strNonce, strlen(strNonce));
    sprintf(value, "default-src 'self'; script-src 'self' 'unsafe-inline' 'nonce-%s'", encodedNonce);

    if(r){
        apr_table_set(r->headers_out, "Content-Security-Policy", value);
        return encodedNonce;
    }
    return "";
}

/* modify this function to insert the nonce using this http://www.apachetutor.org/dev/brigades*/
static apr_status_t script_nonce_out_filter(ap_filter_t *f,
                                        apr_bucket_brigade *bb)
{
  char *pat_text = "$@$";
  char replacement[50];
  strcpy(replacement, sc_add_header(f->r));

  const apr_strmatch_pattern* pattern =  apr_strmatch_precompile(f->r->pool, pat_text, 0);

  if (APR_BRIGADE_EMPTY(bb)) {
     return APR_SUCCESS;
  }
  
  size_t patlen = strlen(pat_text);
  size_t replen = strlen(replacement);

  apr_bucket* b ;
  apr_bucket* tmp_b;

  for ( b = APR_BRIGADE_FIRST(bb);
  b != APR_BRIGADE_SENTINEL(bb);
  b = APR_BUCKET_NEXT(b) ) {

    const char* buf ;
    size_t bytes;
    size_t rem_len;

    if (APR_BUCKET_IS_EOS(b)) {
      continue;
    }
    
    if ( apr_bucket_read(b, &buf, &bytes, APR_BLOCK_READ)
            == APR_SUCCESS ) {
      
      /* We have a bucket full of text.  Just escape it where necessary */
      size_t count = 0 ;
      const char* p = buf;
      char *match = NULL;
      rem_len = bytes;
      
      while ( count < bytes ) {
      //while(1) {
        //size_t sz = strcspn(p, "<>&\"") ;
        match = apr_strmatch(pattern, p, rem_len);
        //match = strstr(p, pat_text);

        if(match){
          size_t sz = (size_t)(match - p);

          count += sz;

          if ( count < bytes ) {
            apr_bucket_split(b, sz) ; //Split off before buffer
            b = APR_BUCKET_NEXT(b) ;  //Skip over before buffer
            tmp_b = apr_bucket_transient_create(replacement, replen, f->r->connection->bucket_alloc);
            APR_BUCKET_INSERT_BEFORE(b, tmp_b); //Insert the replacement
            apr_bucket_split(b, patlen);  //Split off the pattern to remove
            APR_BUCKET_REMOVE(b);   //... and remove it
            b = APR_BUCKET_NEXT(b);  //Move cursor on to what-remains
                  //so that it stays in sequence with
                  //our main loop
            //int inc = abs(replen - patlen);
            /*
            while(1){
              ;
            }
            */
            count += patlen; //inc;
            p += (sz + patlen); //inc;
            rem_len = (bytes - count);
            match = NULL;
          }
        }
        else{
          break;
        }
      }
    }
  }
  return ap_pass_brigade(f->next, bb);
}



static void script_nonce_register_hooks(apr_pool_t *p) {
    ap_hook_insert_filter(script_nonce_insert_filter,NULL,NULL,APR_HOOK_LAST);
    ap_register_output_filter(s_szscript_nonce_name,script_nonce_out_filter,NULL,
                              AP_FTYPE_RESOURCE);
}

module AP_MODULE_DECLARE_DATA script_nonce_module = {
    STANDARD20_MODULE_STUFF,
    NULL,                     /* per-directory config creator */
    NULL,                     /* dir config merger */
    NULL,                     /* server config creator */
    NULL,                     /* server config merger */
    NULL,                     /* command table */
    script_nonce_register_hooks, /* set up other request processing hooks */
};
