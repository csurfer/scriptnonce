\documentclass[11pt]{article}
\title{\textbf{Script-Nonce apache2 module to prevent Cross-site scripting attacks}}
\author{Nipun Valoor (108802890)\\
		Vinyas Maddi (108824449)\\
		Vishwas Badrinath Sharma (108692460)}
\date{}
\usepackage{graphicx}
\begin{document}

\maketitle

\section{Abstract}

In this project we are coming up with a robust design and implementation of a server side (on Apache Platform) script nonce that can be used to prevent cross-site scripting (XSS) attacks. Deliverable includes a ready to be deployed Apache Module which prevents cross-site scripting attacks (Stored and Reflective) and a brief report on the performance of a server running our module.

\section{Introduction}

\subsection{Cross-site Scripting Attacks}

\emph{Cross-site scripting (XSS) is a type of computer security vulnerability typically found in Web applications. XSS enables attackers to inject client-side script into Web pages viewed by other users}\footnote{http://en.wikipedia.org/wiki/Cross-site\_scripting}.\\
\emph{An XSS vulnerability arises when Web applications take data from users and dynamically include it in Web pages without first properly validating the data. XSS vulnerabilities allow an attacker to execute arbitrary commands and display arbitrary content in a victim user's browse}\footnote{http://www.veracode.com/security/xss}.\\\\
These are broadly classified into two categories.
\begin{enumerate}
	\item \textbf{Stored/Persistent} In this kind of attack, an evil script gets stored in the database and gets executed on each of the victims who happen to access a webpage which tries to retrieve the same and display. 
	\item \textbf{Reflective/Non-Persistent} In Reflective XSS attack, usually a victim follows a malignant link set up by the attacker and which in turn results in an evil script to be run. See the sample attack in a later section to understand better.
\end{enumerate}

\subsection{Design}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.35]{/home/csurfer/scriptnonce/report/Design_Method1.png}
\caption{Character by character replacement in the buffer}
\label{}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.35]{Design.png}
\caption{Pattern replacement using bucket splits}
\label{}
\end{figure}

\pagebreak

The first major design decision was to figure out how to efficiently replace patterns in the files being served by the server. Following are the two choices we had
\begin{itemize}
	\item Copy the contents of the bucket into a temporary buffer and perform pattern replacement there. Following the pattern replacement we create a new bucket with this buffer as the content and insert it back into the parent brigade(\emph{Please refer Figure 1}).
	\item Identifying the bucket containing the pattern and splitting the bucket before and after the pattern. We drop the hence created middle bucket and in its place insert a new bucket containing the current nonce value(\emph{Please refer Figure 2}).
\end{itemize}

We choose to go with the second option because it not only reduces the overhead of copying data from bucket to buffer unnecessarily(pattern to be matched not found in bucket scenario) but provides us with an efficient way to replace the pattern with variable length nonce.\\
During benchmarking we found out that the choice to go with the second option was indeed the right one(\emph{Please refer Figure 10 and Figure 12}).

\subsection{Implementation Hurdles}

During our initial attempts at benchmarking the performance of the module we observed that there was a memory leak. The graph below indicates that the memory consumed by Apache was increasing linearly with time(pink line in the graph below) until it could increase no more. At this point we also observed that the response times spiked and the server stopped responding.

\begin{figure}[htp]
\centering
\includegraphics[scale=0.26]{Script1000_perf_resptimes_memleak.png}
\caption{Memory Leak statistics}
\label{}
\end{figure}

\subsubsection{Fixing the memory leak}
A thorough code review was done. We identified the cause of the leak to be the macro APR\_BUCKET\_REMOVE() which we were using to remove the bucket with the magic string. This macro does not free the memory associated with the bucket.\\
We replaced APR\_BUCKET\_REMOVE() with the function apr\_bucket\_delete() which not only removes the bucket but also frees associated memory.

\section{Software/Browser Configuration Required}

Following software need to be installed.
\begin{enumerate}
	\item Apache2 (sudo apt-get install apache2)
	\item apxs2 (sudo apt-get install apache2-prefork-dev)
	\item Google Chrome version  28.0.1500.5 Dev since it supports CSP.
\end{enumerate}

\section{How To Use: Developers Guide}
\begin{enumerate}
	\item Determine a magic parameter to use to differentiate evil scripts from good scripts. This must be kept a secret internal to the developers, if the attacker gets his hand on this, the whole security of our system is compromised. Our module searches for this particular string in a script and replaces it with a random nonce generated by the module ( generated on each request received by the server).
	\item Insert this value in a configuration file named scripts\_nonce.conf  in \emph{/etc/apache2/mods-available}.
	\item Compile and insert the module(c  file provided, see bit bucket link) by using the following command.
	\begin{center}apxs -i -a -c mod\_example.c\end{center}
	\item Restart the apache server
	\begin{center}sudo service apache2 restart\end{center}
\end{enumerate}

\section{Testing}

\subsection{Proof of Correctness}
All the tests were carried out on a webserver running on a laptop with the following configuration : RAM: 6GB; Processor: Intel Pentium Core I5,2.5Ghz; OS: Ubuntu 12.04; Webserver: Apache2.2; Network Connection: 100MBps LAN

For testing the module, we developed a few webpages which did basic functions like inserting, querying a MySQL DB and displaying content on the webpage.(Please see testing.php, getxss.php from the bit bucket repo).

\subsubsection{Security against Stored XSS}

To demonstrate how the module developed successfully blocks evil scripts stored in a Database from execution, we did the following test cases and obtained the following results.

\begin{enumerate}
	\item An external html file containing a bunch of good scripts having the correct nonce is called from the file testing.php and we can observe that the CSP enabled browser allows these scripts to be executed.
	\item When we enter a user comment in the text area, it gets stored in the database and is then retrieved and displayed on the webpage. If the user enters an evil script without the nonce/with an incorrect nonce, it does not get executed as it violates the CSP.
	\item A developer who knows the secret nonce (the magic parameter) can enter a valid script as comment on the webpage and that script gets executed since the magic parameter will be replaced with the correct nonce by our module.
\end{enumerate}

\subsubsection{Security against Reflective XSS}

To demonstrate how the module developed successfully prevents Reflective XSS attacks we did the following test cases and obtained the following results.

\begin{enumerate}
	\item We created a webpage (please see getxss.php) that uses the GET method. An attacker can enter a evil script as a search query on this webpage (which is intended to display the comments entered by a specific user from the database (search query is the username)), copy the link generated from the address bar and then trick a victim to actually click on this link (by sending via a bogus email etc). On a webserver that does not run our module, this will cause the evil script to execute on the clients browser. But our module effectively prevents this attack as the nonce (If it exists) does not match the secret magic parameter and hence will not be replaced with the current valid nonce.
\end{enumerate}

\pagebreak

\section{Benchmarking}

All the following benchmarking tests were done with an html file containing 1000 inline scripts. This essentially means that the module finds and replaces 1000 times for every page that is requested. File size is approximately 80 Kilo Bytes.\\
Each inline script contains the attribute nonce followed by the magic string. Every occurance of this magic string is replaced by our module with a script nonce. The same nonce is also added to the response header.\\\\
Tool used for performance testing : Apache JMeter.
\begin{itemize}
\item Section 6.1 indicates the performance of the server when the script-nonce module is disabled. \\
\item Section 6.2 indicates the performance of the server under various user loads with script-nonce module enabled.\\
\end{itemize}

\pagebreak

\subsection{Without script nonce module}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.30]{Script1000_100usr_perf_resptimes_WM.png}
\caption{Server metrics and response times(Max concurrent users 100)}
\label{}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.30]{Script1000_1000usr_perf_resptimes_nomodule.png}
\caption{Server metrics and response times(Max concurrent users 1000)}
\label{}
\end{figure}

\pagebreak

\subsection{With script nonce module}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.33]{Script1000_100usr_perf_resptimes.png}
\caption{Server metrics and response times(Max concurrent users 100)}
\label{}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.35]{Script1000_100usr_resptime_threads.png}
\caption{Response times (ms) vs No of concurrent users(Max 100)}
\label{}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.35]{Script1000_200usr_perf_resptimes.png}
\caption{Server metrics and response times(Max concurrent users 200)}
\label{}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.35]{Script1000_200usr_resptime_threads.png}
\caption{Response times (ms) vs No of concurrent users(Max 200)}
\label{}
\end{figure} 

\begin{figure}[htp]
\centering
\includegraphics[scale=0.35]{Script1000_1000usr_perf_resptimes.png}
\caption{Server metrics and response times(Max concurrent users 1000)}
\label{}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.35]{Script1000_1000usr_resptimes.png}
\caption{Response times (ms) vs No of concurrent users(Max 1000)}
\label{}
\end{figure}

\begin{figure}[htp]
\centering
\includegraphics[scale=0.35]{Script1000_1000usr_perf_resptimes_oldCode.png}
\caption{Character by character replacement response times (ms) vs No of concurrent users(Max 1000)}
\label{}
\end{figure}

\pagebreak

\section{Codebase}
The code base can be found on Bitbucket at the link given below\\
\emph{https://bitbucket.org/csurfer/scriptnonce}

\section{Conclusion}
In this project we have successfully implemented an Apache module capable of dynamically inserting script nonces. Our module can be configured with custom magic strings and variable length nonces.\\
Post benchmarking we found that there was not a significant difference between response times observed when the module that uses simple character replacement was in place as against when module used bucket splitting.\\
We have successfully demonstrated that using this module we can prevent several types of XSS attacks. For moderate user loads(Upto 100 concurrent users) our module does not have any significant performance overhead.
\end{document}