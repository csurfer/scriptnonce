#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""template.py: Description of what the module does."""

from optparse import OptionParser
import logging
import sys
from io import open
from os import path

__author__ = "Vishwas B Sharma"
__email__ = "sharma.vishwas88@gmail.com"

LOG_FORMAT = "%(asctime).19s %(levelname)s %(filename)s: %(lineno)s %(message)s"

def main(options, args):
  print "<html>"
  print "<body>"
  print "This page contains "+options.num + " scripts";
  
  for i in range(int(options.num)):
    if options.nonce and options.dyn:
      print "<script nonce=\"05d4cccd291b545110aa80010dc4409e\">"
    elif options.nonce:
      print "<script nonce=\"1234\">"
    else:
      print "<script>"
    print "alert(\"Hello World "+str(i)+"\");"
    print "</script>"

  print "</body>"
  print "</html>"
  

def debug(type_, value, tb):
  if hasattr(sys, 'ps1') or not sys.stderr.isatty():
    # we are in interactive mode or we don't have a tty-like
    # device, so we call the default hook
    sys.__excepthook__(type_, value, tb)
  else:
    import traceback, pdb
    # we are NOT in interactive mode, print the exception...
    traceback.print_exception(type_, value, tb)
    print("\n")
    # ...then start the debugger in post-mortem mode.
    pdb.pm()

if __name__ == "__main__":
  parser = OptionParser()
  parser.add_option("-f", "--file", dest="filename", help="Input file")
  parser.add_option("-n", dest="num", help="Number of js")
  parser.add_option("--nonce", dest="nonce", action="store_true", default=False, help="Need nonce?")
  parser.add_option("--dyn", dest="dyn", action="store_true", default=False, help="Need nonce?")
  parser.add_option("-l", "--log", dest="log", help="log verbosity level",
                    default="INFO")
  (options, args) = parser.parse_args()
  if options.log == 'DEBUG':
    sys.excepthook = debug
  numeric_level = getattr(logging, options.log.upper(), None)
  logging.basicConfig(level=numeric_level, format=LOG_FORMAT)
  main(options, args)
